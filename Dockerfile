FROM python:3.6

ADD . /code

WORKDIR /code

RUN set export FLASK_APP=app.py

RUN pip install -r requirements.txt

CMD  python -m flask run --host=0.0.0.0