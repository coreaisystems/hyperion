################Hyperion User Interface MicroService#########################

To see the functionality please follow following steps:

1) CD to the project folder

2) Create a python3 virtualenvironment
	virtualenv -p python3 venv

3) Activate it
	source vene/bin/activate

4) Install all requirements
	pip install -r requirements.txt

5)Run the app
	python app.py

You app will be running on : 
http://0.0.0.0:8000/ 



