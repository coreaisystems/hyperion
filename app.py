#!flask/bin/python
from flask import Flask, jsonify,  render_template
from flask import request


app = Flask(__name__,static_url_path='/static')


#index page
@app.route('/')
def index():
   return render_template('login.html')

#sign up page
@app.route('/signup/')
def register():
   return render_template('register.html')

#forgot Password page
@app.route('/forgotpassword/')
def forgotpassword():
   return render_template('forgot-password.html')

#Reset Password page
@app.route('/resetpassword/')
def resetpassword():
   return render_template('reset-password.html')

#Dashboard page
@app.route('/dashboard/')
def dashboard():
   return render_template('home.html')

#Service Selection page
@app.route('/service-selection/')
def service_selection():
   return render_template('service_selection.html')

#loaddata page
@app.route('/loaddata/')
def load_data():
   return render_template('load-data.html')

#train generator page
@app.route('/traingenerator/')
def train_generator():
   return render_template('train-generator.html')

#generate text page
@app.route('/generatetext/')
def generate_text():
   return render_template('generate-text.html')

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=8000)