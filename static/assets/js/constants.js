var auth_apis = {
    signup: "http://127.0.0.1:5000/api/v1/registration",
    login: "http://127.0.0.1:5000/api/v1/login",
    logout: "http://127.0.0.1:5000/api/v1/logout",
    reset_password : "http://127.0.0.1:5000/api/v1/reset_password",
    reset_password_token : "http://127.0.0.1:5000/api/v1/reset_password/<token>",
    getLoggedInUser : "http://127.0.0.1:5000/api/v1/getUserData"
};



var service_apis = {
    service_selection : "http://127.0.0.1:5000/api/v1/service_selection",
    service_dashboard : "http://127.0.0.1:8080/superdashboard",
    getUserServices : "http://127.0.0.1:8080/api/v1/getUserServices",
};


var document_parser_api = {
	document_parser : "http://127.0.0.1:3000/documentparser/api/v1.0/getCSV"
}