
$(document).ready(function(){
	/* Signup function*/
	$('#create_account').click(function(){
		var name = $('#user_name').val();
		var email = $('#user_email').val();
		var password = $('#user_password').val();
		var cnf_password = $('#user_cnf_password').val();
		var agree_tc = $('#customCheckRegister').is(":checked")
		if(agree_tc == false){
			$("#check_box_error").show().delay(5000).fadeOut();
			return false;
		}
		if((name == '') || (email == '') || (password == '') || (cnf_password == '')){
			$("#empty_fields_error").show().delay(5000).fadeOut();
		}else{
			console.log('all found')
			is_valid_email = isEmail(email)
			console.log('is_valid_email',is_valid_email)
			if(is_valid_email == true){
				if(password.length >= 8){
					if(password != cnf_password){
						$("#password_mismatch_error").show().delay(5000).fadeOut();
					}else{
						console.log('start ajax here')
						$.ajax({
						     type: 'POST',
						     url: auth_apis.signup,
						     data: {
						     	'name':name,
						     	'email':email,
						     	'password':password
						     },
						     success: function(data) {    
						        console.log('data',data)
						        console.log('stat---',data.status)
						        if(data.status == 'success'){
						        	$("#register_success").show().delay(5000).fadeOut();
							        $('#user_name').val('');
									$('#user_email').val('');
									$('#user_password').val('');
									$('#user_cnf_password').val('');
						        }else{
						        	$('#server_error').html(data.message);
						        	$("#server_error").show().delay(5000).fadeOut();
						        }
						        
						     },
						     failure: function(data) { 
						         alert('Got an error dude');
						     }
						});
					}
				}else{
					$("#password_length_error").show().delay(5000).fadeOut();
				}
			}else{
				$("#invalid_email_error").show().delay(5000).fadeOut();
			}
		}
	})


	/* Login function*/
	$('#login_button').click(function(){
		var email = $('#login_email').val();
		var password = $('#login_password').val();
		if((email == '') || (password == '')){
			$("#empty_fields_error").show().delay(5000).fadeOut();
		}else{
			is_valid_email = isEmail(email)
			console.log('is_valid_email',is_valid_email)
			if(is_valid_email == true){
				console.log('start ajax here')
				$.ajax({
				     type: 'POST',
				     url: auth_apis.login,
				     data: {
				     	'email':email,
				     	'password':password
				     },
				     success: function(data) {    
				        console.log('data',data)
				        console.log('stat---',data.status)
				        if(data.status == 'success'){
				        	console.log('login success')
				        	console.log('login success>>',data.account)
				        	console.log('login success1>>',data.account[1])
				        	var username = data.account[1]
				        	var key = data.account[4]
				        	//document.getElementById("username_span").innerHTML = username;
				        	window.location.href="/service-selection?unique_key="+key
				        	//document.getElementById("username_span").innerHTML = username;
				        }else{
				        	$('#server_error').html(data.message);
				        	$("#server_error").show().delay(5000).fadeOut();
				        }
				     },
				     failure: function(data) { 
				         alert('Got an error dude');
				     }
					});
			}else{
				$("#invalid_email_error").show().delay(5000).fadeOut();
			}
		}
	})


	/* Logout function*/
	$('#logout_button').click(function(){
		$.ajax({
		     type: 'GET',
		     url: auth_apis.logout,
		     success: function(data) {    
		        console.log('data',data)
		        console.log('stat---',data.status)
		        if(data.status == 'success'){
		        	console.log('logout success')
		        	window.location.href="/"
		        }
		     },
		     failure: function(data) { 
		         alert('Got an error');
		     }
		});	
	})


	/* Forgot Password function*/
	$('#search_email_button').click(function(){
		var email = $('#user_email_forgot').val();
		if(email == ''){
			$("#invalid_email_error").show().delay(5000).fadeOut();
		}else{
			is_valid_email = isEmail(email)
			console.log('is_valid_email',is_valid_email)
			if(is_valid_email == true){
				console.log('start ajax here')
				$.ajax({
				     type: 'POST',
				     url: auth_apis.reset_password,
				     data: {
				     	'email':email,
				     },
				     success: function(data) {    
				        console.log('data',data)
				        console.log('stat---',data.status)
				        if(data.status == 'success'){
				        	$("#email_sent_success").show().delay(5000).fadeOut();
				        }else{
				        	$('#server_error').html(data.message);
				        	$("#server_error").show().delay(5000).fadeOut();
				        }
				     },
				     failure: function(data) { 
				         alert('Got an error dude');
				     }
					});
			}else{
				$("#invalid_email_error").show().delay(5000).fadeOut();
			}
		}
	})

	/* Reset Password function*/
	$('#password_reset_button').click(function(){
		var password = $('#reset_password').val();
		var cnf_password = $('#reset_cnf_password').val();
		if((password == '') || (cnf_password == '')){
			$("#empty_fields_error").show().delay(5000).fadeOut();
		}else{
			if(password.length >= 8){
				if(password != cnf_password){
					$("#password_mismatch_error").show().delay(5000).fadeOut();
				}else{
					url = window.location.href
					var str_split = url.split("=")
					var token = str_split[1]
					$.ajax({
					     type: 'POST',
					     url: auth_apis.reset_password_token,
					     data: {
					     	'password':password,
					     	'token':token
					     },
					     success: function(data) {    
					        if(data.status == 'success'){
					        	$("#change_success").show().delay(5000).fadeOut();
					        }else{
					        	$('#server_error').html(data.message);
					        	$("#server_error").show().delay(5000).fadeOut();
					        }					        
					     },
					     failure: function(data) { 
					         alert('Got an error dude');
					     }
					});
				}
			}else{
				$("#password_length_error").show().delay(5000).fadeOut();
			}
		}
	})

	/*Restrict numbers in name field*/
	$("#user_name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });

    /* Validate email address*/
    function isEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

	/* Service Selection */
	$('#service_selection').click(function(){
		var hyp_check = $('#hyperion').is(":checked")
		var auto_ml = $('#autoML').is(":checked")
		if((hyp_check == false) && (auto_ml == false)){
			$("#empty_fields_error").show().delay(5000).fadeOut();
		}else{
			console.log('selected')
			var selected = [];
            $.each($("input[name='service_name']:checked"), function(){
                selected.push($(this).val());
            });
            var query = window.location.search.substring(1);
			var qs = parse_query_string(query);
            console.log('selected',selected)
            var data = {
			     	'selected_services':selected,
			     	'query_string':qs,
			     }
			var key = qs.unique_key
            $.ajax({
			     type: 'POST',
			     contentType: 'application/json',
			     url: service_apis.service_selection,
			     dataType : 'json',
			     data : JSON.stringify(data),
			     success: function(data) {   
			     	console.log('data',data) 
			        if(data.status == 'success'){
			        	window.location.href=service_apis.service_dashboard+"/?unique_key="+key
			        }else{
			        	$('#server_error').html(data.message);
			        	$("#server_error").show().delay(5000).fadeOut();
			        }					        
			     },
			     failure: function(data) { 
			         alert('Got an error dude');
			     }
			});
		}
	})

	/* get unique_id */
	function parse_query_string(query) {
	  var vars = query.split("&");
	  var query_string = {};
	  for (var i = 0; i < vars.length; i++) {
	    var pair = vars[i].split("=");
	    var key = decodeURIComponent(pair[0]);
	    var value = decodeURIComponent(pair[1]);
	    // If first entry with this name
	    if (typeof query_string[key] === "undefined") {
	      query_string[key] = decodeURIComponent(value);
	      // If second entry with this name
	    } else if (typeof query_string[key] === "string") {
	      var arr = [query_string[key], decodeURIComponent(value)];
	      query_string[key] = arr;
	      // If third or later entry with this name
	    } else {
	      query_string[key].push(decodeURIComponent(value));
	    }
	  }
	  return query_string;
	}


})